import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:test_task_creonit/repository/data_models/categories.dart';

class CategoriesDB {
  CategoriesDB._init();
  static final CategoriesDB db = CategoriesDB._init();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  /* initDB() async {
    return await openDatabase(join(await getDatabasesPath(), 'categoriesDB.db'),
        onCreate: (db, version) async {
      await db.execute('''CREATE TABLE categories ("
            "id INTEGER PRIMARY KEY,"
            "title TEXT,"
            "slug TEXT,"
            ")
      ''');
    });
  }
 */
  initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "categoriesDB.db");
    print(path);
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int versions) async {
        await db.execute('''CREATE TABLE categories ("
            "id INTEGER PRIMARY KEY,"
            "title TEXT,"
            "slug TEXT,"
            ")''');
      },
    );
  }

  newCategories(/* Categories  */ newCategories) async {
    final db = await database;
    /* var res = await db.query("categories", where: "id=?", whereArgs: [id]) */
    var res = await db.rawInsert("INSERT INTO categories (id,title,slug)"
        "VALUES (?,?,?)");
    return res;
  }

/* ${newCategories.items.first.id}, ${newCategories.items.first.title},${newCategories.items.first.slug} */
  getAllCategories() async {
    final db = await database;
    var res = await db.query("categories");
    List<Categories> list =
        res.isNotEmpty ? res.map((e) => Categories.fromJson(e)).toList() : [];
    return list;
  }
}
