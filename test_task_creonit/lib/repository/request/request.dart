import 'dart:convert';

import 'package:test_task_creonit/repository/data_models/categories.dart';
import 'package:http/http.dart' as http;
import 'package:test_task_creonit/repository/data_models/products.dart';
import 'package:test_task_creonit/repository/database/database';

class DataRequest {
  Future<Categories> fetchCategories() async {
    try {
      final response = await http.get(
          Uri.parse('https://vue-study.skillbox.cc/api/productCategories'));
      /* CategoriesDB.db.newCategories(jsonDecode(response.body)); */
      return Categories.fromJson(jsonDecode(response.body));
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<Products> fetchProducts(String productID) async {
    try {
      final responseProducts = await http.get(Uri.parse(
          'https://vue-study.skillbox.cc/api/products?categoryId=$productID'));
      return Products.fromJson(jsonDecode(responseProducts.body));
    } catch (e) {
      throw Exception(e);
    }
  }
}
