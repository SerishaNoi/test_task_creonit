// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'colors.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductColor _$ProductColorFromJson(Map<String, dynamic> json) => ProductColor(
      id: json['id'] as int,
      title: json['title'] as String,
      code: json['code'] as String,
    );

Map<String, dynamic> _$ProductColorToJson(ProductColor instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'code': instance.code,
    };
