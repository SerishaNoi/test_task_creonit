// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categoriesitems.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoriesItems _$CategoriesItemsFromJson(Map<String, dynamic> json) =>
    CategoriesItems(
      id: json['id'] as int,
      title: json['title'] as String,
      slug: json['slug'] as String,
    );

Map<String, dynamic> _$CategoriesItemsToJson(CategoriesItems instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'slug': instance.slug,
    };
