import 'package:json_annotation/json_annotation.dart';
import 'package:test_task_creonit/repository/data_models/categoriesitems.dart';

part 'categories.g.dart';

@JsonSerializable(explicitToJson: true)
class Categories {
  Categories({required this.items});

  final List<CategoriesItems> items;

  factory Categories.fromJson(Map<String, dynamic> json) =>
      _$CategoriesFromJson(json);

  Map<String, dynamic> toJson() => _$CategoriesToJson(this);
}
