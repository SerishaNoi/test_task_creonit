import 'package:json_annotation/json_annotation.dart';

part 'file.g.dart';

@JsonSerializable()
class FileClass {
  FileClass({
    required this.url,
    required this.name,
    required this.originalName,
    required this.extension,
    required this.size,
  });

  final String url;
  final String name;
  final String originalName;
  final String extension;
  final String size;

  factory FileClass.fromJson(Map<String, dynamic> json) =>
      _$FileClassFromJson(json);

  Map<String, dynamic> toJson() => _$FileClassToJson(this);
}
