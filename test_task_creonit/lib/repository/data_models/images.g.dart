// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductImages _$ProductImagesFromJson(Map<String, dynamic> json) =>
    ProductImages(
      file: FileClass.fromJson(json['file'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProductImagesToJson(ProductImages instance) =>
    <String, dynamic>{
      'file': instance.file.toJson(),
    };
