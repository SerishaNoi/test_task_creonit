import 'package:json_annotation/json_annotation.dart';
import 'package:test_task_creonit/repository/data_models/file.dart';

part 'images.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductImages {
  ProductImages({
    required this.file,
  });

  final FileClass file;

  factory ProductImages.fromJson(Map<String, dynamic> json) =>
      _$ProductImagesFromJson(json);

  Map<String, dynamic> toJson() => _$ProductImagesToJson(this);
}
