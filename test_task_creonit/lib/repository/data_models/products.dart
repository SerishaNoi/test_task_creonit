import 'package:json_annotation/json_annotation.dart';
import 'package:test_task_creonit/repository/data_models/items.dart';

part 'products.g.dart';

@JsonSerializable(explicitToJson: true)
class Products {
  Products({
    required this.items,
  });

  final List<Item> items;

  factory Products.fromJson(Map<String, dynamic> json) =>
      _$ProductsFromJson(json);

  Map<String, dynamic> toJson() => _$ProductsToJson(this);
}
