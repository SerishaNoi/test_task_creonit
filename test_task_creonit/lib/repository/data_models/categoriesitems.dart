import 'package:json_annotation/json_annotation.dart';

part 'categoriesitems.g.dart';

@JsonSerializable()
class CategoriesItems {
  final int id;
  final String title;
  final String slug;

  const CategoriesItems(
      {required this.id, required this.title, required this.slug});

  factory CategoriesItems.fromJson(Map<String, dynamic> json) =>
      _$CategoriesItemsFromJson(json);

  Map<String, dynamic> toJson() => _$CategoriesItemsToJson(this);
}
