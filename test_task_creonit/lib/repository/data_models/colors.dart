import 'package:json_annotation/json_annotation.dart';

part 'colors.g.dart';

@JsonSerializable()
class ProductColor {
  ProductColor({
    required this.id,
    required this.title,
    required this.code,
  });

  final int id;
  final String title;
  final String code;

  factory ProductColor.fromJson(Map<String, dynamic> json) =>
      _$ProductColorFromJson(json);

  Map<String, dynamic> toJson() => _$ProductColorToJson(this);
}
