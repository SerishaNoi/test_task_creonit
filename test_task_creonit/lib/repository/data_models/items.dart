import 'package:json_annotation/json_annotation.dart';
import 'package:test_task_creonit/repository/data_models/colors.dart';
import 'package:test_task_creonit/repository/data_models/images.dart';

part 'items.g.dart';

@JsonSerializable(explicitToJson: true)
class Item {
  Item({
    required this.id,
    required this.title,
    required this.slug,
    required this.image,
    required this.price,
    required this.colors,
  });

  final int id;
  final String title;
  final String slug;
  final ProductImages image;
  final int price;
  final List<ProductColor> colors;

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  Map<String, dynamic> toJson() => _$ItemToJson(this);
}
