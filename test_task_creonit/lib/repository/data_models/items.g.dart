// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'items.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) => Item(
      id: json['id'] as int,
      title: json['title'] as String,
      slug: json['slug'] as String,
      image: ProductImages.fromJson(json['image'] as Map<String, dynamic>),
      price: json['price'] as int,
      colors: (json['colors'] as List<dynamic>)
          .map((e) => ProductColor.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'slug': instance.slug,
      'image': instance.image.toJson(),
      'price': instance.price,
      'colors': instance.colors.map((e) => e.toJson()).toList(),
    };
