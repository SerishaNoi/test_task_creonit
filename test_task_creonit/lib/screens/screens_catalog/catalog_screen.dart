import 'package:flutter/material.dart';
import 'package:test_task_creonit/repository/data_models/categories.dart';
import 'package:test_task_creonit/repository/request/request.dart';
import 'package:test_task_creonit/screens/screens_products/products_screen.dart';

class CatalogScreen extends StatefulWidget {
  const CatalogScreen({Key? key}) : super(key: key);

  @override
  _CatalogScreenState createState() => _CatalogScreenState();
}

class _CatalogScreenState extends State<CatalogScreen> {
  Future<Categories> categories = DataRequest().fetchCategories();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new),
          color: const Color(0xff414951),
          onPressed: () {},
        ),
        elevation: 0.4,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Text(
          'Каталог',
          style: TextStyle(color: Color(0xff414951), fontSize: 18),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: FutureBuilder<Categories>(
          future: categories,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.items.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 0.5,
                            color: Color(0xff8A8884),
                          ),
                        ),
                      ),
                      constraints:
                          const BoxConstraints.tightFor(width: 363, height: 52),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            snapshot.data?.items[index].title ?? '',
                            style: const TextStyle(
                                color: Color(0xff414951), fontSize: 17),
                          ),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: Color(0xff8a8884),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => ItemsPage(
                            appBarText: snapshot.data!.items[index].title,
                            categoriesID:
                                snapshot.data!.items[index].id.toString(),
                          ),
                        ),
                      );
                    },
                  );
                },
              );
            }
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
