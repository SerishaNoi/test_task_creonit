import 'package:flutter/material.dart';
import 'package:test_task_creonit/main.dart';
import 'package:test_task_creonit/repository/data_models/products.dart';
import 'package:test_task_creonit/repository/request/request.dart';
import 'package:test_task_creonit/screens/screens_catalog/catalog_screen.dart';

class ItemsPage extends StatefulWidget {
  final String categoriesID;
  final String appBarText;
  const ItemsPage(
      {Key? key, required this.categoriesID, required this.appBarText})
      : super(key: key);

  @override
  _ItemsPageState createState() => _ItemsPageState();
}

class _ItemsPageState extends State<ItemsPage> {
  final ScrollController _scrollController = ScrollController();
  List<int> cartItemsId = [];
  late final Future<Products> product;

  @override
  void initState() {
    super.initState();
    product = DataRequest().fetchProducts(widget.categoriesID);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Center(
          child: Text(
            widget.appBarText,
            style: const TextStyle(color: Color(0xff414951), fontSize: 17),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(46),
          child: Container(
            height: 46,
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1, color: Color(0xffe3e3e2)),
                top: BorderSide(width: 1, color: Color(0xffe3e3e2)),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: SizedBox(
                    child: TextButton.icon(
                        style: TextButton.styleFrom(
                            primary: const Color(0xff414951)),
                        onPressed: () {},
                        icon: const Icon(Icons.tune),
                        label: const Text('Фильтры')),
                  ),
                ),
                Container(
                  height: 26,
                  decoration: const BoxDecoration(
                    border: Border(
                      right: BorderSide(
                        width: 1,
                        color: Color(0xffe3e3e2),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: SizedBox(
                    child: TextButton.icon(
                      style: TextButton.styleFrom(
                          primary: const Color(0xff414951)),
                      onPressed: () {},
                      icon: const Icon(Icons.import_export_sharp),
                      label: const Text('По популярности'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: const Color(0xff414951),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.search,
            ),
            color: const Color(0xff414951),
            onPressed: () {},
          ),
        ],
      ),
      body: FutureBuilder<Products>(
        future: product,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return GridView.builder(
              padding: const EdgeInsets.all(14),
              scrollDirection: Axis.vertical,
              itemCount: snapshot.data?.items.length,
              controller: _scrollController,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: 15,
                crossAxisSpacing: 15,
                childAspectRatio: 0.60,
                crossAxisCount: 2,
              ),
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Stack(
                      children: [
                        SizedBox(
                          height: 164,
                          width: 164,
                          child: Image.network(snapshot
                              .data!.items[index].image.file.url), //Картинки
                        ),
                        Positioned(
                          bottom: 124,
                          right: -1,
                          child: IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.favorite_border_rounded,
                              size: 26,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    SizedBox(
                      width: 164,
                      height: 34,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: const Color(0xfff6f6f6),
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8))),
                        onPressed: () {
                          final itemID =
                              snapshot.data!.items.elementAt(index).id;
                          setState(
                            () {
                              if (cartItemsId.contains(itemID)) {
                                cartItemsId.remove(itemID);
                              } else {
                                cartItemsId.add(itemID);
                              }
                            },
                          );
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              snapshot.data!.items[index].price // Цена из API
                                  .toString(),
                              style: const TextStyle(
                                color: Color(0xff414951),
                              ),
                            ),
                            Icon(
                              Icons.shopping_cart_outlined,
                              color: cartItemsId.contains(
                                      snapshot.data!.items.elementAt(index).id)
                                  ? const Color(0xff3eb280)
                                  : const Color(0xff414951),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Text(
                          snapshot.data!.items[index].title), // Названия из API
                    ),
                  ],
                );
              },
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
