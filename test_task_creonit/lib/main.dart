import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_task_creonit/screens/other_screens/basket.dart';
import 'package:test_task_creonit/screens/other_screens/favorite.dart';
import 'package:test_task_creonit/screens/other_screens/main_screen.dart';
import 'package:test_task_creonit/screens/other_screens/profile.dart';
import 'package:test_task_creonit/screens/screens_catalog/catalog_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(const NavigationScreen());
}

class NavigationScreen extends StatefulWidget {
  const NavigationScreen({Key? key}) : super(key: key);

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  int selectedIndex = 0;
  void _selectedTab(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  static const List<Widget> _listOfWidgets = <Widget>[
    MainScreen(),
    CatalogScreen(),
    FavoriteScreen(),
    BasketScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: _listOfWidgets.elementAt(selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          onTap: _selectedTab,
          currentIndex: selectedIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          selectedItemColor: const Color(0xff414951),
          unselectedItemColor: const Color(0xff8A8884),
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                label: 'Главная'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.grid_view,
                ),
                label: 'Каталог'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite_outline_outlined,
                ),
                label: 'Избранное'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.shopping_cart_outlined,
                ),
                label: 'Корзина'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.person_outline,
                ),
                label: 'Профиль'),
          ],
        ),
      ),
    );
  }
}
